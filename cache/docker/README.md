# Running scripts in the Docker environment

## Usage

1. Build the image with `docker build -t equilibrator-cache:latest .`. Please
    first ensure that you have downloaded the Marvin Debian package 
    `marvin_linux_XX.Y.deb` and have 
    a license from ChemAxon. Put both in the `docker/marvin/` directory.
2. You can work interactively with the container: `docker run -it 
    equilibrator-cache:latest` try, for example, `python3 
    scripts/populate_cache.py --help`.

## Warning

Data inside of a container is fleeting. If you require data persistence, 
consider [mounting a volume](https://docs.docker.com/storage/volumes/).
