#!/usr/bin/env bash

# The MIT License (MIT)
#
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

set -eu

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
MARVIN=$(find "${SCRIPTPATH}/marvin" -maxdepth 1 -name 'marvin_linux*.deb' -print -quit)
LICENSE="${SCRIPTPATH}/marvin/license.cxl"

if [[ !( -f "${MARVIN}" && -f "${LICENSE}") ]]; then
    echo "Could not find the Marvin Debian package and/or license.cxl file." >&2
    exit 1
else
    echo "Found $(basename ${MARVIN})." >&2
    echo "Found $(basename ${LICENSE})." >&2
fi
