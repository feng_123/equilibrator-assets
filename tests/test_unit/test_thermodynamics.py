"""Tests for microspecies calculations."""
# The MIT License (MIT)
#
# Copyright (c) 2020 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from typing import List

import pytest
from equilibrator_cache import Compound

from equilibrator_assets.thermodynamics import create_microspecies_mappings


@pytest.mark.parametrize(
    "atom_bag, dissociation_constants, expected_ddg_over_rts, "
    "expected_major_ms_number_protons, expected_major_ms_charge",
    [
        (  # adenine
            {"N": 5, "C": 5, "H": 5, "e-": 70},
            [9.84, 2.51],
            [22.65, 0.0, -5.8],
            5,
            0,
        ),
        (  # acetate
            {"C": 2, "O": 2, "H": 3, "e-": 32},
            [4.54],
            [0.0, -10.45],
            3,
            -1,
        ),
        ({"C": 6, "H": 6, "e-": 42}, [], [0.0], 6, 0),  # benzene:
    ],
)
def test_microspecies(
    atom_bag: dict,
    dissociation_constants: List[float],
    expected_ddg_over_rts: List[float],
    expected_major_ms_number_protons: int,
    expected_major_ms_charge: int,
) -> None:
    """Test creating microspecies from the Compound object."""

    # create a Compound object for acetate
    cpd = Compound(
        atom_bag=atom_bag, dissociation_constants=dissociation_constants
    )
    microspecies_mappings = create_microspecies_mappings(cpd, mid_ph=7.0)

    for ms, expected in zip(microspecies_mappings, expected_ddg_over_rts):
        assert ms["ddg_over_rt"] == pytest.approx(expected, abs=0.1)
        if ms["is_major"]:
            assert ms["number_protons"] == expected_major_ms_number_protons
            assert ms["charge"] == expected_major_ms_charge
